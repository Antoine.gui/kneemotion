# Documentation interne 

Ce répertoire contient l'ensemble de votre documentation entre autre :

* Votre bibliographie/état de l'art sur le projet.
* Des documents pertinents pour votre travail (issus par exemple de votre état de l'art).

N'hésitez pas à en ajouter ou mettre des fichiers directement dans la documentation (**à vous de vous organiser**).

Vous pouvez donc ajouter d'autres fichiers et/ou dossiers afin de compléter la documentation, par exemple, des explications de votre code/de vos algorithmes, des références informatiques externes (cf. tutorial sur python/git, source de morceau de codes, etc)

Soulignons qu'il s'agit donc de la documentation interne (non public). Pour la rendre disponible au public, vous devez aller dans ce [dossier](../www).
