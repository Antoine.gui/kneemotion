# Bibliographie

Votre état de l'art/bibliographie essentiellement constitué de quelques phrases clefs (_bullet points_/résumé synthétique) et surtout des références ! (de préférence en MarkDown).

Basé sur le cahier des charges, quelques fichiers MarkDown d'exemples ont été inclus mais n'hésitez pas à en rajouter ou à les découper en un nombre plus grand !

**Organisez-vous !**

