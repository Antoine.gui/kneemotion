# Marqueurs

Si vous comptez utiliser des marqueurs pour résoudre votre problème ?

Dans l'affirmatif, faire la liste de marqueurs envisageables avec leurs avantages/inconvénients.

Justifier le choix et aussi la position où vous les mettriez !
