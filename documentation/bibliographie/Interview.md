# Interview avec un (ou des) praticien(s) (optionnel)

## Liste des questions 


Quelques points :

* Préparer votre interview avec une série de questions (Q1, Q2, etc).
* Présenter le contexte au kiné/médecin.
* Demander si vous pouvez éventuellement le citer nominativement ou s'il veut conserver son anonymat (?).

1. Q1
2. ...

## Réponse Kiné 1

Présentation Anonyme ou pas ?

1. R1
2. ...

Autres remarques/informations/idées données par le kiné

* idée1
* info1
* etc.



