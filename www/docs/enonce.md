# Analyse de mouvements avec une caméra 3D

![OAK-D Caméra](img/oak-d.png){align=right}

Dans le domaine biomédical, l'analyse de mouvements permet, au médecin ou au kinésithérapeute, d'identifier et d'objectiver le diagnostic d'une pathologie du mouvement. Elle permet ensuite de suivre l'évolution de cette pathologie au cours de séances de rééducation. 

Dans le cadre de ce projet d'année, une caméra 3D stéréoscopique [OAK-D](https://shop.luxonis.com/products/oak-d) sera fournie et utilisée afin de suivre, modéliser et quantifier des mouvements, plus particulièrement, ceux du genou lors d'exercices de flexion-extension.


##  Cahier des charges 

* réaliser une recherche bibliographique des principes de fonctionnement des caméras de profondeur (3D), principalement les caméras stéréoscopiques comme l'OAK-D.
* rédiger un (court) état de l'art des différents exercices de rééducation du genou ainsi que leurs modélisations et quantifications pouvant être pertinentes pour le kinésithérapeute. Cet état de l'art pourrait être éventuellement complété par l'interview d'un (ou de) praticien(s) dans le domaine. 
* sur base du [_Starter Kit_](#code-de-depart) fourni pour ce projet, développer un programme python permettant de quantifier le mouvement d'extension-flexion du genou avec les contraintes suivantes :
![Flexion-extension du genou en position assise](img/exercice_genou.jpg){align=right}
    * la caméra est fixe et orientée de manière optimale pour capturer le mouvement du patient assis sur une chaise. L'environnement d'acquisition peut être également parfaitement contrôlé (luminosité, mobilier, etc).
    * des marqueurs peuvent être placés sur le patient afin d'aider au suivi. Veillez toutefois à justifier le choix et la position des marqueurs !
    * une phase d'initialisation de votre algorithme est autorisée au cours de laquelle le patient peut être placé dans une position fixe pré-déterminée et/ou des paramètres de votre algorithme peuvent être ajustés manuellement.
    * la durée totale d'installation du patient (avec placement éventuel de marqueurs) et de la phase d'initialisation pour un exercice devra être la plus courte possible, de préférence inférieure à la minute.
    * la durée totale de votre algorithme devra être la plus courte possible afin de pouvoir traiter au minimum 10 images par seconde après la phase éventuelle d'initialisation sur une station de travail récente (voir [Matériel](#configuration-ordinateur)). Il conviendra donc d'utiliser au maximum des fonctions de librairies dont le principe de fonctionnement devra être bien compris.
    * les paramètres, extraits au cours du temps, devront être enregistrés et analysés à la fin de l'exercice et un récapitulatif devra être fourni au praticien (par exemple, durée de l'exercice, statistiques et graphiques des données acquises, valeurs pertinentes, comparaison de la séance d'exercices par rapport à des précédentes afin d'évaluer le progrès, etc). Notons que si la durée de traitement le permet, les paramètres mesurés pourraient être également affichés dans un graphique au cours du temps.
    * la création et l'utilisation d'enregistrements d'exercices (_dataset_) au cours du développement sont fortement conseillées.
* tester et valider votre algorithme en le comparant méthodiquement et quantitativement à d'autres solutions matérielles (dispositifs) et/ou logicielles (programmes) et/ou algorithmiques (code trouvé dans des ressources externes et/ou dans la littérature qui pourra remplacer votre code de traitement).
* les logiciels développés ainsi que les tests et validations doivent être documentés et placés dans un répositoire partagé (voir [ci-après](#répositoire-gitlab)). Les tests et la validation peuvent être mis en ligne avec des images/vidéos (sur youtube, etc.) grâce à l'utilisation des [_gitlab pages_](https://docs.gitlab.com/ee/user/project/pages/).
* créer une vidéo finale récapitulative de votre projet (à mettre en ligne, de préférence sur youtube) montrant l'application des algorithmes implémentés pour le suivi de l'exercice de flexion-extension du genou ainsi que la validation de votre solution.

Bien entendu les options reprises ci-dessus peuvent être étendues et/ou améliorées, il faut faire preuve d’imagination. Par exemple, des analyses d'autres exercices de rééducation pourraient éventuellement être réalisées sur base de vos algorithmes en les adaptant un peu si nécessaire.

Afin de pouvoir réaliser votre projet dans de bonnes conditions, vous trouverez à l'[adresse suivante](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/documentation/) une documentation importante que nous vous invitons à lire car elle apporte des compléments d'informations très précieux pour le déroulement du projet. Notez que cette documentation pourrait faire encore l'objet de petites modifications au cours du premier semestre. Nous vous conseillons donc de la revoir de temps en temps.


## Répositoire gitlab

Dans le cadre du projet de cette année, un répositoire sur [_gitlab_](https://gitlab.com) devra être utilisé pour divers aspects de la réalisation de votre projet :

1. Documentation pour vos recherches bibliographiques.
2. Le développement de vos codes informatiques.
3. La publicité de vos tests/validations avec l’aide de pages webs générées à partir de fichiers _MarkDown_ (utilisation de _MkDocs_ avec les [_gitlab pages_](https://docs.gitlab.com/ee/user/project/pages/)). Des liens vers des vidéos externes (youtube ou autre) devraient être  idéalement inclus. 
4. La gestion en continue de votre projet par le biais de “status reports” rédigés avec des fichiers _MarkDown_ et l’inclusion de votre “Risks and Issue Logs” (cf. Gestion de Projet par Patrick Simon).
5. Remonter les problèmes à l’équipe organisatrice du projet et/ou à votre tuteur.

Diverses informations complémentaires vous sont fournies sur [une page de la documentation en ligne du projet](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/documentation/gitlab/). Un [template de projet “git”](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2022-2023/kneemotion) est également partagé sur gitlab que vous pourrez cloner ou faire un _fork_ dans votre groupe _BIOMEDx_ (_x_ = 1 à 4, donc votre numéro de groupe). La version définitive de ce template sera prête début octobre mais n'hésitez pas à survoler déjà ce projet gitlab qui vous donne de nombreuses indications.

Vous serez invité d'ici fin septembre dans votre groupe privé _BIOMEDx_ avec votre adresse e-mail ULB. Vous devrez alors créer un compte sur [_gitlab.com_](https://gitlab.com) si vous n'en avez pas encore un.


## Matériel et logiciel

Afin de pouvoir réaliser le projet, une caméra [OAK-D](https://shop.luxonis.com/products/oak-d) sera prêtée par groupe d'étudiants. Elle devra être remise à la fin du projet en parfait état de fonctionnement et avec tous ses accessoires. Du matériel complémentaire pourra être utilisé (ordinateurs, ...) au LISA ou fourni sur demande précise. Tout autre matériel nécessaire à la réalisation du projet devra être acheté, fabriqué et/ou récupéré par le groupe avec un budget maximum de 100€ (p/ex marqueurs, etc).

### Configuration ordinateur

Afin de pouvoir travailler dans de bonnes conditions, la configuration minimale conseillée de l'ordinateur sur lequel le projet devrait être réalisé est la suivante :

* Processeur avec une fréquence minimale de ~3GHz (idéalement i3, i5, i7 min génération 4 ou équivalent AMD).
* Mémoire vive de minimum 8GB Ram.
* Port(s) USB 3.0.
* Disque dur SSD avec au moins 50GB d'espace libre.

Le LISA dispose d'une salle informatique avec stations de travail répondant à ces critères (plus précisément [i7-7800X @3.5GHz](https://www.intel.com/content/www/us/en/products/sku/123589/intel-core-i77800x-xseries-processor-8-25m-cache-up-to-4-00-ghz/specifications.html) et 32GB Ram) et auquel vous pourrez avoir accès en dehors des heures de travaux pratiques. Les performances de vos algorithmes seront évaluées sur ces ordinateurs qui serviront de référence. L'horaire et les modalités d'accès sont précisés sur le [site de la documentation en ligne](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/documentation/lisapcroom/).

### Code de départ

Un code python de départ (_Starter Kit_) permettant d'enregistrer et de restituer les données d'une caméra OAK-D est fourni dans le template du Projet BA2 2022-2023 dont vous pouvez faire un _fork_ ou "cloner" à l'[adresse suivante](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2022-2023/kneemotion). Le code du lecteur pourra donc intégrer vos algorithmes. Veuillez lire le _README_ du dossier `src` (_Starter Kit_) qui fournit l'ensemble des instructions pour son utilisation ou [la documentation en ligne du projet](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/documentation/) qui reprend ce _README_ à la [page _Starter Kit_](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/documentation/starter_kit/). 


## Personnes ressources

- Recherche bibliographique : [Axel Dero](mailto:axel.dero@ulb.be)
- Support "OAK-D" Windows/Ubuntu : [Rudy Ercek](mailto:rudy.ercek@ulb.be)
- Programmation python : [Olivier Debeir](mailto:olivier.debeir@ulb.be), [Eline Soetens](mailto:eline.soetens@ulb.be), [Rudy Ercek](mailto:rudy.ercek@ulb.be) 
- Gestion de versions avec _git_ : [Eline Soetens](mailto:eline.soetens@ulb.be), [Laurie Van Bogaert](mailto:laurie.van.bogaert@ulb.be), [Olivier Debeir](mailto:olivier.debeir@ulb.be)
- Gestion des groupes : [Rudy Ercek](mailto:rudy.ercek@ulb.be), [Eline Soetens](mailto:eline.soetens@ulb.be), [Laurie Van Bogaert](mailto:laurie.van.bogaert@ulb.be), [Maxime Verstraeten](mailto:maxime.verstraeten@ulb.be)
- Gestion de projet : [Patrick Simon](mailto:patrick.simon@ulb.be)
- Electronique : [Michel Osée](mailto:michel.osee@ulb.be), [Geoffrey Vanbienne](mailto:geoffrey.vanbienne@ulb.be)


## Calendrier

Voir guide du projet dans l'[Université Virtuelle](http://uv.ulb.ac.be)


**Date de la dernière révision de l'énoncé :**  {{ git_revision_date }}

