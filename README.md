# Template du projet BA2 Biomed 2022-2023 avec _Starter Kit_ (ULB Polytech)

Il s'agit d'un template pour créer votre répositoire dans le cadre du [projet BA2 Biomed 2022-2023](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/enonce) et qui contient le code de départ de votre programme dans le dossier `src` c.à.d. le _Starter Kit_. Il suffit de faire un "_fork_" de ce projet vers un autre projet de votre groupe _BIOMEDx_ dans gitlab.

![forker](www/docs/img/fork.png)

Un répositoire sert principalement au stockage de fichiers _textes_, **le stockage de fichiers _binaires_ (vidéos, grosses images, etc.) devra donc être fortement limité**.

En pratique, la structure proposée, sous forme de dossiers, est la suivante :
- `documentation` contient l'ensemble de votre documentation interne (vos recherches, vos références (liens), etc.).
- `gestion` contient l'ensemble des vos fichiers relatifs à la gestion de projet : "_Status Reports_" en Markdown, "risk and issues logs" en xlsx et votre fichier _GanttProject_ mis à jour régulièrement. 
- `src` contient le code python de base i.e. _Starter Kit_ permettant l'enregistrement, la lecture et le traitement des données acquises avec la caméra 3D [OAK-D](https://shop.luxonis.com/products/oak-d). Une partie de ce code (fichier `process.py`) devra inclure vos algorithmes d'analyse du mouvement d'extension-flexion du genou. Veuillez lire le [README](src/README.md) de ce dossier pour plus d'informations.
- `www` contient la documentation mise à disposition du public, typiquement la méthodologie et un résumé de vos validations et tests avec images et vidéos externes. Il utilise le framework [MkDocs](http://www.mkdocs.org/) pour mettre en ligne le contenu.

Vous trouverez en général dans chacun de ces dossiers un _README_ qui décrit plus spécifiquement son contenu.

**N'hésitez pas à modifier ce template par rapport à vos besoins !**

**Version définitive du template prévue le 03/10/2022** mais n'hésitez pas à déjà le tester et le parcourir. 
