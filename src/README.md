# _Starter Kit_

## Introduction

Le "_Starter Kit_" du projet BA2 2022-2023 intitulé "[Analyse de mouvements avec une caméra 3D](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/enonce)" et contenu dans le dossier `src` du [template du projet](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2022-2023/kneemotion) comprend 3 programmes principaux qui gèrent l'acquisition avec une caméra [OAK-D](https://shop.luxonis.com/products/oak-d) :

1. `record.py` permettant l'enregistrement de séquences des caméras stéréoscopiques OAK-D (couleur en BGR "_color_", profondeur "_depth_" et optionnellement disparité "_disparity_") dans un fichier au format [HDF5](https://www.hdfgroup.org/).
2. `convert.py`permettant la conversion, la compression et l'extraction de morceaux de fichiers enregistrés afin de réduire leurs tailles et/ou d'augmenter les performances de lecture.
3. `play.py` permettant de jouer, soit une séquence pré-enregistrée, soit un flux en _live_ de la caméra OAK-D afin de l'afficher et/ou d'appliquer vos algorithmes par la modification du fichier `process.py`.

Les codes de ces fichiers ont été inspirés en partie d'exemples fournis par "[luxonis](https://www.luxonis.com/)" dans deux répositoires [github](https://github.com/luxonis) :

* Exemples de [DepthAI Python Library](https://github.com/luxonis/depthai-python), plus précisément [StereoDepth](https://github.com/luxonis/depthai-python/tree/main/examples/StereoDepth)
* Exemples de [depthai-experiments](https://github.com/luxonis/depthai-experiments), plus précisément [RGB-D projection Demo](https://github.com/luxonis/depthai-experiments/tree/master/gen2-pointcloud/rgbd-pointcloud) où la fichier `projector_3d.py` a été repris dans ce "_Starter Kit_" pour l'affichage 3D.

Notons que ces programmes ont été spécifiquement conçus pour ce projet car il n'existe pas de solutions d'acquisition permettant de rejouer une séquence acquise sans qu'une caméra OAK-D soit connectée au PC, cf. [_gen2-record-replay_](https://github.com/luxonis/depthai-experiments/tree/master/gen2-record-replay).


## Installation 

Dans un environnement _python 3.9_, de préférence, [virtuel sous anaconda](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2022-2023/documentation/anaconda/#travailler-sous-un-environnement-virtuel), les librairies du fichier `requirements.txt` doivent être installées avec la commande `pip install -r requirements.txt` dans une console (i.e. cmd/powershell/terminal/anaconda prompt suivant le système d'exploitation utilisé). Il est également possible d'exécuter le fichier `install.py` avec la commande `python install.py` qui lance la commande précédente dans l'environnement actif. En cas de problèmes avec cette installation, le fichier `requirements_versions.txt` contient les librairies avec les versions qui ont été utilisées lors du développement de ce _Starter Kit_ et qui peuvent donc être installées avec la commande `pip install -r requirements_versions.txt`. Vous pouvez également les installer avec la commande `python install_versions.py`.

Notons que certaines librairies installées ne sont pas nécessairement utiles pour la réalisation de votre projet car elles ont des fonctionnalités redondantes avec d'autres. Vous pouvez également utiliser d'autres librairies python lors de votre développement, qui devront donc être ajoutées au fichier `requirements.txt`.


## L'enregistreur `record.py`

Par défaut, la commande `python record.py` démarre l'enregistrement dans le dossier courant. Le nom du fichier est généré à partir de la date et heure d'acquisition, en pratique avec le format "%Y%m%d - %H%M.hdf5". Par exemple, l'enregistrement du fichier "20220819 - 1032.hdf5" a été démarré le 19 août 2022 à 10h32.

C'est également le comportement par défaut lorsque le fichier `record.py` est exécuté dans un environnement de développement (_IDE_), par exemple [_PyCharm_](https://www.jetbrains.com/fr-fr/pycharm/).

Il est possible de modifier la configuration de l'enregistrement (options, ...) :

* Avec un fichier de configuration [_JSON_](https://en.wikipedia.org/wiki/JSON), par défaut nommé "config_record.json" dans le répertoire courant.
* Avec quelques arguments permettant de modifier, à la volée, certains paramètres du fichier de configuration mais surtout de nommer le fichier d'enregistrement de la séquence.

Afin d'**arrêter un enregistrement** avant sa fin éventuellement planifiée, **la touche _q_** (_**q**uit_) doit être appuyée, parfois à plusieurs reprises, sur une fenêtre d'affichage (_color_, _depth_  ou _disparity_). **Notez que si vous arrêtez l'enregistrement de manière forcée, le fichier HDF5 sera incomplet et donc inutilisable avec le lecteur [`play.py`](#le-lecteur-playpy) !** 

Nous conseillons de placer les enregistrements dans un sous-répertoire de ce dossier, par exemple _recordings_, en spécifiant par l'attribut _path_ de la section [`recording` du fichier de configuration _JSON_](#section-recording).

### Configuration JSON de l'enregistreur

Le fichier de configuration _JSON_, par défaut "config_record.json", est composé de deux niveaux d'indentation. Le premier niveau que nous nommerons "section" contient 3 attributs [`recording`](#section-recording), [`display`](#section-display) et [`stereodepth`](#section-stereodepth) que nous détaillons ci-après. Notons que les types et surtout les valeurs associés à un fichier _JSON_ ne sont pas une transposition exacte de ceux utilisés dans python, par exemple une valeur python "None" doit être "null" en _JSON_, "False" devient "false" et les chaînes de caractères sont mises, de préférence, entre "guillements".

#### Section `recording`

Cette section `recording` contient les paramètres relatifs aux fichiers d'enregistrement HDF5. Le tableau suivant reprend les différents attributs de cette section.

| Attributs     | Valeur<br/>défaut | Type | Description                                                                                     |
|---------------|-------------------|------|-------------------------------------------------------------------------------------------------|
| _disparity_   | false             | bool | Enregistre la disparité ou non dans le fichier HDF5 (par défaut non).                           |
| _colortojpg_  | true              | bool | Active la compression des images en jpg dans le fichier HDF5.                                   |
| _frame_cnt_   | 0                 | int  | Nombre maximum d'"images" (_frames_/trames) à acquérir, par défaut 0, sans limite.              |
| _duration_    | 0                 | int  | Durée maximale de l'enregistrement en secondes, par défaut 0, sans limite.                      |
| _path_        | ""                | str  | Chemin de destination des fichiers enregistrés avec un nom automatique.                         |
| _compression_ | null              | str  | Compression de données dans l'HDF5 : null (pas de compression), compresseur "lzf" ou "gzip".    |
| _comp_opts_   | null              | int  | Niveau de compression pour "gzip", valeurs de 0 à 9 c.à.d. du plus bas au plus haut.            |
| _nbdigit_     | 5                 | int  | Nombre minimum de chiffres pour le numéro de trame (groupe d'un _dataset_) dans le fichier HDF5.|

#### Section `display`

Cette section `display` contient les options d'affichage de certaines fenêtres pendant l'enregistrement (cf. tableau ci-dessous).

| Attributs     | Valeur<br/>défaut | Type | Description                                                                                            |
|---------------|-------------------|------|--------------------------------------------------------------------------------------------------------|
| _disparity_   | false             | bool | Affiche/masque la disparité.                                                                           |
| _depth_       | false             | bool | Affiche/masque la profondeur.                                                                          |
| _color_       | true              | bool | Affiche/masque la couleur.                                                                             |
| _3D_          | false             | bool | Affiche/masque le nuage de points 3D. Laisser toujours à false pour un enregistrement.                 |
| _maxRange_    | 0                 | int  | Distance maximale à afficher pour la profondeur et la 3D (0 sans limite).                              |
| _ifpsnbframe_ | 30                | int  | Nombre de trames nécessaires pour calculer un _framerate_ instantané et l'afficher sur l'image couleur.|

Pour limiter les soucis lors d'un enregistrement, nous conseillons d'afficher une seule fenêtre, soit la couleur, soit la profondeur, ce qui permet également d'arrêter un enregistrement avant sa fin programmée avec la touche _q_. Notons que pour éviter un enregistrement beaucoup trop lent, le nuage de points 3D ne doit surtout pas être affiché si vous voulez un enregistrement avec un nombre d'images par seconde suffisant !

#### Section `stereodepth`

La section `stereodepth` contient les paramètres liés à l'acquisition stéréoscopique de la caméra OAK-D, à son [_pipeline_](https://docs.luxonis.com/projects/api/en/latest/components/nodes/stereo_depth) que nous vous conseillons de lire attentivement. En effet, ce sont des paramètres importants qui peuvent influencer fortement la qualité de la carte de profondeur (distance min/max, précision, etc.). Le tableau ci-dessous décrit les différents attributs disponibles.

| Attributs        | Valeur<br/>défaut | Type | Description                                                                                                                                                                                                                                                                                               |
|------------------|-------------------|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| _fps_            | 30                | int  | Nombre d'images par seconde de la caméra couleur.                                                                                                                                                                                                                                                         |
| _conf_thr_       | 245               | int  | Seuil de confiance pour la valeur de profondeur entre 0 et 255. Au plus cette valeur est faible, meilleure sera la précision des valeurs de profondeur, en contrepartie, avec moins de points valides c.à.d. plus de trous dans la carte de profondeur.                                                   |
| _lrcheck_        | true              | bool | Meilleur traitement des occlusions en comparant les disparités de la caméra gauche vers celle de droite aux disparités obtenues dans l'autre direction.                                                                                                                                                   |
| _extended_       | false             | bool | Permet de diminuer la distance minimale d'un objet à la caméra.                                                                                                                                                                                                                                           |
| _subpixel_       | true              | bool | Permet d'obtenir une meilleure précision sur les longues distances.                                                                                                                                                                                                                                       |
| _monoResolution_ | "720p"            | str  | Résolution à laquelle les cartes de disparité et profondeur sont calculées ("400p","720p" ou "800p").                                                                                                                                                                                                     |
| _downScaleColor_ | true              | bool | La résolution de la caméra couleur est réduite de 1080p à 720p. Notons que la résolution des cartes de disparité et profondeur sont ajusté à celle de la caméra couleur, donc en 720p si vrai et 1080p si faux. En pratique, nous déconseillons de travailler en 1080p pour des questions de performance. |
| **Filtres**      |                   |      | **Voir [_pipeline_](https://docs.luxonis.com/projects/api/en/latest/components/nodes/stereo_depth) et [configuration Stereo Depth](https://docs.luxonis.com/projects/api/en/latest/components/messages/stereo_depth_config/#stereodepthconfig)**                                                          |
| _minRange_       | 10                | int  | Distance minimale pour la profondeur (en mm), les valeurs en dessous de cette profondeur sont invalidées c.à.d. mises à 0.                                                                                                                                                                                |
| _maxRange_       | 15000             | int  | Distance maximale pour la profondeur (en mm), les valeurs au dessus de cette profondeur sont invalidées c.à.d. mises à 0.                                                                                                                                                                                 |
| _medfilt_        | "7x7"             | str  | Taille du noyau (_kernel_) du filtre median : "off" (désactivé),"3x3","5x5" et "7x7".                                                                                                                                                                                                                     |
| _temporalFilter_ | false             | bool | Filtre temporel. Si actif, la vitesse d'acquisition diminue.                                                                                                                                                                                                                                              |
| _spatialFilter_  | false             | bool | Filtre spatial. Si actif, la vitesse d'acquisition diminue.                                                                                                                                                                                                                                               |

### Arguments de `record.py`

Lorsque vous exécutez le programme `record.py` dans un _shell_, vous pouvez spécifier plusieurs arguments en option à la commande `python record.py` dont voici la liste

| Argument    | Alternatif | Description                                                                                  |
|-------------|------------|----------------------------------------------------------------------------------------------|
| --file      | -f         | Nom du fichier d'enregistrement HDF5.                                                        |
| --config    | -c         | Nom du fichier de configuration JSON, sinon, il utilise config_record.json par défaut.       |
| --template  | -t         | Génère un fichier de configuration JSON avec les valeurs par défaut que vous pouvez modifier.|
| --frame_cnt | -fc        | Nombre maximum d'images de l'enregistrement (0 pas de limite).                               |
| --duration  | -d         | Durée maximale de l'enregistrement en secondes (0 pas de limite).                            |
| --fps       | -fps       | Nombre d'images par seconde de la caméra couleur.                                            |
| --help      | -h         | Affiche l'aide.                                                                              |     

Si les arguments _frame_cnt_, _duration_ ou _fps_ sont spécifiés, leurs valeurs seront toujours prioritaires par rapport à celles du fichier de configuration JSON.

Quelques exemples d'utilisations de `record.py` en ligne de commande :

* `python record.py -f test.hdf5 -d 10` enregistre une séquence dans un fichier "test.hdf5" avec une durée maximale de 10 secondes.
* `python record.py -t myconfig.json` génère un fichier de configuration "myconfig.json" que vous pouvez modifier à votre guise.
* `python record.py -c myconfig.json` enregistre une séquence dans un fichier "%Y%m%d - %H%M.hdf5" en utilisant le fichier de configuration "myconfig.json" s'il existe.


## Le convertisseur `convert.py`

Le programme `convert.py` permet de convertir un fichier HDF5 d'enregistrement vers :

* un autre fichier HDF5 par exemple, d'une durée plus courte, une (dé)compression de certaines données, etc.
* des fichiers indépendants au format _jpg_ pour l'image couleur et en données textes formattées [_CSV_](https://fr.wikipedia.org/wiki/Comma-separated_values) pour la profondeur et la disparité. Ces fichiers utiliseront une nomenclature avec des chiffres correspondant au numéro de la trame, par défaut `%05d.jpg` pour _color_ , `%05d.depth.csv` pour _depth_ et `%05d.disparity.csv` pour _disparity_, donc pour la trame 200, les fichiers seront respectivements `00200.jpg`, `00200.depth.csv` et `00200.disparity.csv`. A la fin de l'exportation, un fichier JSON nommé `export.json` contiendra toutes les informations du fichier d'enregistrement source ainsi que des informations complémentaires liées à la conversion. Nous laissons le soin au lecteur d'identifier les informations utiles pouvant être intéressantes lors de l'utilisation d'une trame exportée. 

Ce convertisseur utilise uniquement la ligne de commande où vous devez spécifier différents arguments en fonction de vos besoins. 

| Argument     | Alternatif | Description                                                                                                                                          |
|--------------|------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| --file       | -f         | Nom du fichier d'enregistrement HDF5 à convertir.                                                                                                    |
| --outfile    | -o         | Nom du nouveau fichier de sortie HDF5.                                                                                                               |
| --path       | -p         | Répertoire de sortie pour des fichiers indépendants (_jpg_ et _CSV_).                                                                                |
| --comp       | -c         | Utilisation d'une compression des données internes du fichier HDF5, par défaut, null (pas de compression), gzip ou lzf.                              |
| --comp_level | -l         | Niveau de la compression gzip de 0 à 9 (4 par défaut si gzip).                                                                                       |
| --start      | -s         | Numéro de la première image (_frame_/trame) à convertir.                                                                                             |
| --end        | -e         | Numéro de la dernière image (_frame_/trame) à convertir (0 pour la dernière image du fichier source).                                                |
| --data       | -d         | Données à convertir, par défaut "rgbd" c.à.d. couleur et profondeur, "rgb" couleur (pas pour HDF5) et "all" pour ajouter la disparité.               |
| --jpg        | -j         | Activer la conversion en jpg dans l'hdf5, par défaut "idem" donc comme le fichier source, sinon "true" pour l'activer ou "false" pour la désactiver. |
| --help       | -h         | Affiche l'aide.                                                                                                                                      |

Quelques exemples d'utilisation de `convert.py` en ligne de commande : 

* `python convert.py -f input.hdf5 -o output.hdf5 -s 100 -e 200 -d "all" -j false` convertit les trames 100 à 200 (donc 101 trames) du fichier "input.hdf5" vers un fichier "output.hdf5" sans compression jpg ou autre mais avec toutes les données présentes c.à.d. disparité incluse si elle est contenue dans "input.hdf5".
* `python convert.py -f input.hdf5 -o output.hdf5 -s 200 -j true -c "gzip" -l 9` convertit le fichier "input.hdf5" à partir de la trame 200 en un fichier "output.hdf5" avec les images en jpg et les profondeurs compressées en gzip avec le meilleur taux de compression, ce qui permet de réduire fortement la taille du fichier source.
* `python convert.py -f input.hdf5 -p output_dir -d "all"` convertit le fichier input.hdf5 en images jpg et données csv de profondeur et disparité si elle existe dans le dossier "output_dir".


## Le lecteur `play.py`

Ce programme permet la lecture de fichiers HDF5 enregistrés avec `record.py` mais également de récupérer les images directement en _live_ de la caméra OAK-D afin de les afficher mais surtout de les traiter avec le fichier "process.py".

Comme pour le programme `record.py`, il est possible de configurer le lecteur :

* Avec un fichier de configuration _JSON_, par défaut nommé "config_play.json" dans le répertoire courant.
* Avec quelques arguments passés à la commande `python play.py`.

### Configuration _JSON_ du lecteur 

Le fichier de configuration _JSON_, par défaut "config_play.json", est composé de deux niveaux d'indentation. Le premier niveau que nous nommerons "section" contient 4 attributs [`playing`](#section-playing), [`process`](#section-process), [`display`](#section-display) et [`stereodepth`](#section-stereodepth). Les deux dernières sections sont identiques à celles du fichier de [configuration _JSON_ de l'enregistreur](#configuration-json-de-l-enregistreur) et ne seront donc pas décrites dans les sections suivantes.

#### Section `playing`

Cette section contient 3 attributs relatifs à la lecture des fichiers enregistrés qui sont décrits dans le tableau ci-dessous.

| Attributs | Valeur<br/>défaut | Type  | Description                                                                    |
|-----------|-------------------|-------|--------------------------------------------------------------------------------|
| _file_    | ""                | str   | Nom du fichier HDF5 à lire, vide par défaut c.à.d. flux _live de la caméra.    |
| _speed_   | 1                 | float | Vitesse de lecture souhaitée du fichier, par défaut, à 1 c.à.d. vitesse réelle.|
| _loop_    | false             | bool  | Lecture du fichier en boucle si vrai i.e. répète la lecture indéfiniment.      |

#### Section `process`

Cette section permet de configurer le traitement réalisé par le fichier `process.py`. En pratique, ces attributs permettent à ce stade de générer une vidéo et des images d'un enregistrement avec des informations complémentaires dessus, typiquement le _framerate_ et le numéro de la trame.   

| Attributs        | Valeur<br/>défaut | Type  | Description                                                                                                                            |
|------------------|-------------------|-------|----------------------------------------------------------------------------------------------------------------------------------------|
| _colorimgpath_   | ""                | str   | Répertoire de destinations des images couleur affichées, par défaut aucun.                                                             |
| _colorvideofile_ | ""                | str   | Nom du fichier vidéo à créer, par défaut aucune video. Exemple : "video.avi".                                                              |
| _colorvideofps_  | 30                | float | Nombre d'images par seconde pour la vidéo en sortie mais qui ne correspondra pas nécessairement à la vitesse de génération de la vidéo.|
| _colorwriteloop_ | 0                 | int   | Numéro de la boucle pour laquelle les images ou la vidéo doivent être générées, par défaut, 0, c.à.d. dès le démarrage.                |


### Arguments de `play.py`

Lorsque vous exécutez le programme `play.py` dans un _shell_, vous pouvez spécifier plusieurs arguments en option à la commande `python play.py` dont voici la liste

| Argument   | Alternatif | Description                                                                                  |
|------------|------------|----------------------------------------------------------------------------------------------|
| --file     | -f         | Nom du fichier HDF5 à lire.                                                                  |
| --config   | -c         | Nom du fichier de configuration JSON, sinon, il utilise config_play.json par défaut.         |
| --template | -t         | Génère un fichier de configuration JSON avec les valeurs par défaut que vous pouvez modifier.|
| --speed    | -s         | Vitesse de lecture du fichier.                                                               |
| --loop     | -l         | Lecture du fichier en boucle (true, True, 1) ou non (autre valeur, false, ...).              |
| --fps      | -fps       | Nombre d'images par seconde de la caméra couleur si aucun fichier n'est fourni.              |
| --help     | -h         | Affiche l'aide.                                                                              |     

Si les arguments _speed_, _loop_ ou _fps_ sont spécifiés, leurs valeurs seront toujours prioritaires par rapport à celles du fichier de configuration JSON.

### Interractions avec le lecteur

L'appui de touches spécifiques du clavier sur une fenêtre d'affichage active (sauf 3D) permet de contrôler la lecture :

| Touche  | Action                                                                                                                                                              |
|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **_q_** | Arrête la lecture du fichier ou le flux _live_ de la caméra (_**q**uit_).                                                                                           |
| **_p_** | Pause ou reprend la lecture du fichier uniquement (_**p**ause_/_**p**lay_), le traitement de `process.py` s'effectue cependant encore en continu sur la même trame. |
| **_n_** | En mode pause, passe à la trame suivante (_**n**ext_).                                                                                                              |
| **_b_** | En mode pause, passe à la trame précédente (_**b**efore_).                                                                                                          |


## Affichage et traitement avec `process.py`

Le fichier `process.py` contient une _class_ `DisplayProcess` dont un objet (_DProcess_) est instancié dans `play.py` lors de la lecture d'un fichier ou d'un flux _live_. Comme son nom l'indique, cette _class_ s'occupe de l'affichage et du traitement des différentes trames lues (c.à.d. _color_, _depth_ et _disparity_).

En pratique, même si tous les fichiers du _Starter Kit_ peuvent être modifiés, le fichier `process.py` est le seul qui devrait vraiment l'être avec vos algorithmes de traitement et autres fonctions d'affichage.

Les méthodes principales de `DisplayProcess` qui pourront donc être adaptées sont les suivantes :

1. `initKneeProcess` qui est appelée par le constructeur `__init__` pour initialiser vos propres variables et autres fenêtres d'affichage. En pratique, le constructeur est appelé une seule fois avant la lecture des trames dans `play.py`.
2. `KneeProcess` qui est appelée par la méthode `process` exécutée à chaque trame et qui reçoit effectivement les différentes valeurs de la trame courante (c.à.d. _color_, _depth_ et _disparity_) ainsi que son numéro et son [_timestamp_](https://en.wikipedia.org/wiki/Timestamp) d'acquisition. C'est dans cette méthode que vous devriez implémenter vos algorithmes d'analyse.
3. `finishKneeProcess` qui est appelée par la méthode `finish` exécutée à la fin de la lecture du fichier (ou fin de boucle de lecture du fichier) ou du flux _live_ (après avoir quitter avec la touche *_q_*). C'est dans cette méthode que vous devriez afficher les résultats de votre analyse.

L'instanciation d'un objet de `DisplayProcess` reçoit la configuration _JSON_ du lecteur sous forme de _dict python_ qui est accessible dans toutes les méthodes de cette _class_ avec l'attribut _self.config_.

Comme le montre le code donné, en exemple, dans les 3 méthodes pré-citées du _Starter Kit_, il est possible d'initialiser des variables et des fenêtres d'affichage en utilisant une configuration _JSON_ spécifique. Par exemple, la configuration _JSON_ suivante affiche la fenêtre _mywindow_ et crée une vidéo "test.avi" après la première lecture, donc à la deuxième boucle.

```json
{
  "display" : {
    "mywindow": true
  },
  "playing": {
    "file": "test.hdf5",
    "loop": true
  },
  "process" : {
    "myvar": "Un string",
    "mywindowvideo": "test.avi",
    "mywindowimgpath": "",
    "writemywindow": 1
  }
}
```

**Afin de pouvoir réaliser votre projet, nous vous conseillons de lire et de bien comprendre le code du fichier `process.py`.**


## Performances et optimisations

L'ensemble des programmes fournis y compris vos traitements s'exécute sur un seul "coeur" de votre processeur. 

Il est donc possible d'optimiser un peu l'enregistrement et la lecture en fonction de la configuration de votre ordinateur (fréquence du processeur et vitesse de votre disque dur SSD). En pratique, l'enregistrement devrait atteindre les 30 trames par seconde avec un processeur d'au moins 3GHz et un SSD récent en affichant juste l'image couleur. De même, une lecture en mode plus rapide (i.e. _speed_ à 2 du fichier de [configuration JSON](#section-playing)) devrait permettre de dépasser les 30 trames par seconde en affichant juste l'image couleur. Si tel n'est pas le cas et que les vitesses de lecture/écriture du SSD le permettent, vous pouvez désactiver la conversion en jpg de l'image (_colortojpg_ à _false_ dans le fichier de [configuration JSON de l'enregistreur](#section-recording)) en vous assurant que la sauvegarde de la disparité soit bien désactivée (_disparity_ à _false_ de la [section `recording`](#section-recording)). L'alternative est d'utiliser le programme `convert.py` avec la commande `python convert.py -f input.hdf5 -o output.hdf5 -j false` où "input.hdf5" est le fichier source et "output.hdf5" le fichier de destination. Notons que dans ce cas, le fichier "output.hdf5" sera plus volumineux que le fichier "input.hdf5".

**Lors du développement de vos algorithmes, vous serez sans doutes amené à faire des compromis entre vitesse et précision/qualité. N'hésitez donc pas à comparer toutes vos solutions d'implémentations entre elles en utilisant, par exemple, le fichier de configuration _JSON_ pour les activer, désactiver et/ou les paramétrer.**

