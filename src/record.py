#!/usr/bin/env python

"""record.py: record color, depth and disparity from an OAK-D camera in a HDF5 file"""

__author__ = "Rudy Ercek"
__copyright__ = "Copyright 2022 - LISA - Université Libre de Bruxelles"
__credits__ = ["Rudy Ercek"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Rudy Ercek"
__email__ = "rudy.ercek@ulb.be"
__status__ = "Production"

import cv2
import depthai as dai
import numpy as np
import h5py
import json
import argparse
from datetime import datetime
from os.path import exists

# Default configuration for recording
config = {
    'recording':
        {
            'disparity': False,     # Record or not the disparity with depth in the hdf5
            'colortojpg': True,     # Use jpg compression to save images in hdf5 (lossy compression)
            'frame_cnt': 0,         # Maximum number of frame to record (0 no limit --> use q to quit)
            'duration': 0,          # Maximum duration to record (0 no limit --> use q to quit)
            'path': '',             # default path to record when no filename (auto mode) has been specified
            'compression': None,    # Compression method in hdf5 : None, gzip or lzf (except image if jpg)
            'comp_opts': None,      # Compression level only for gzip
            'nbdigit': 5            # minimum number of digits for frame number
        },
    'display':      # display options while playing
        {
            'disparity': False,     # Hide/show disparity window
            'depth': False,         # Hide/show depth window
            'color': True,          # Hide/show color image
            '3D': False,            # Hide/show 3D point cloud
            'maxRange': 0,          # Maximum depth/range limit to display (0 for full range)
            'ifpsnbframe': 30       # Number of frames to compute an instant framerate shown on the color image
        },
    'stereodepth':  # Stereo configuration, see this link
                    # https://docs.luxonis.com/projects/api/en/latest/components/nodes/stereo_depth/?highlight=stereodepth#currently-configurable-blocks
        {
            'fps': 30,                  # Color camera framerate (goal to try to reach when recording)
            'conf_thr': 245,            # confidence threshold value 0..255, lower the confidence value means better depth accuracy
                                        # Higher confidence threshold means disparities with less confidence are accepted too.
            'lrcheck': True,            # Better handling for occlusions
            'extended': False,          # Closer-in minimum depth, disparity range is doubled
            'subpixel': True,           # Better accuracy for longer distance, fractional disparity 8-levels
            'monoResolution': '720p',   # The disparity/depth is computed at this resolution, then upscaled to RGB resolution
                                        # (400p, 720p or 800p)
            'downScaleColor': True,     # Optional. If set (True), the ColorCamera is downscaled from 1080p to 720p.
                                        # Otherwise (False), the aligned depth is automatically upscaled to 1080p
            # Filters for depth computation, see the previous link (depth filters) and the one below :
            # https://docs.luxonis.com/projects/api/en/latest/components/messages/stereo_depth_config/#stereodepthconfig
            'minRange': 10,             # Minimum range for depth (mm)
            'maxRange': 15000,          # Maximum range for depth
            'medfilt': '7x7',           # Median filter, 'off','3x3','5x5','7x7'
            'temporalFilter': False,    # Temporal filtering with optional persistence, acquisitions slower in the camera when enabled
            'spatialFilter': False,     # Spatial Filtering using high-order domain transform, acquisitions in the camera slower when enabled
        },
}

# Class to sync all images (depth, color and disparity) from the camera
class HostSync:
    def __init__(self):
        self.arrays = {}

    def add_msg(self, name, msg):
        if not name in self.arrays:
            self.arrays[name] = []
        # Add msg to array
        self.arrays[name].append({'msg': msg, 'seq': msg.getSequenceNum()})

        synced = {}
        for name, arr in self.arrays.items():
            for i, obj in enumerate(arr):
                if msg.getSequenceNum() == obj['seq']:
                    synced[name] = obj['msg']
                    break
        # If there are 3 (all) synced msgs, remove all old msgs
        # and return synced msgs
        if len(synced) == 3:  # color, depth, nn
            # Remove old msgs
            for name, arr in self.arrays.items():
                for i, obj in enumerate(arr):
                    if obj['seq'] < msg.getSequenceNum():
                        arr.remove(obj)
                    else:
                        break
            return synced
        return False



# Arguments parser with some override configuration (maximum recording duration, ...)
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-f', '--file', default='auto', type=str, help="File to store the captured data")
parser.add_argument('-fps', '--fps', type=float, help='Color camera framerate (fps)')
parser.add_argument('-fc', '--frame_cnt', type=int,
                    help='Maximum number of frames to record')
parser.add_argument('-d', '--duration', type=float,
                    help='Maximum duration (in seconds) to record')
parser.add_argument('-c', '--config', type=str, default='config_record.json',
                    help='Configuration json file')
parser.add_argument('-t', '--template', type=str, help='Generate a default json configuration file')

args = parser.parse_args()

# Write the json template for a configuration file
if args.template is not None:
    with open(args.template, 'w') as f:
        json.dump(config, f, indent=4)
        print('A default json configuration file named "'+args.template+
              '" has been generated ! You can modify it with your settings !')
        exit()

# Read the json configuration file (if it exists)
if exists(args.config):
    with open(args.config, 'r') as f:
        cfg = json.load(f)
        for key, value in cfg.items():
            if key in config.keys():
                config[key] = {**config[key],**value}

# Recording configuration update
if args.file == 'auto': # filename to generate automatically (auto)
    args.file = config['recording']['path']+datetime.now().strftime("%Y%m%d - %H%M.hdf5")
tojpg = config['recording']['colortojpg']
nbmax = config['recording']['frame_cnt']
if args.frame_cnt is not None: nbmax= args.frame_cnt
maxduration = config['recording']['duration']
if args.duration is not None: maxduration= args.duration
comp = config['recording']['compression']
comp_opts = config['recording']['comp_opts']

# show/hide windows (at least display one in order to have a visual feedback when recording)
showDisparity = config['display']['disparity']
showDepth = config['display']['depth']
showColor = config['display']['color']
show3D = config['display']['3D']
curRange = config['display']['maxRange']
if curRange < 1:
    curRange = config['stereodepth']['maxRange']

# Stereo Depth configuration
fps = config['stereodepth']['fps']
if args.fps is not None: fps= args.fps
# Median filter Options: MEDIAN_OFF, KERNEL_3x3, KERNEL_5x5, KERNEL_7x7
median_opt = {
        'off': dai.StereoDepthProperties.MedianFilter.MEDIAN_OFF,
        '3x3': dai.StereoDepthProperties.MedianFilter.KERNEL_3x3,
        '5x5': dai.StereoDepthProperties.MedianFilter.KERNEL_5x5,
        '7x7': dai.StereoDepthProperties.MedianFilter.KERNEL_7x7
}
median = median_opt[config['stereodepth']['medfilt']]
monoResolutionOPT = {
    '400p': dai.MonoCameraProperties.SensorResolution.THE_400_P,
    '480p': dai.MonoCameraProperties.SensorResolution.THE_480_P,
    '720p': dai.MonoCameraProperties.SensorResolution.THE_720_P,
    '800p': dai.MonoCameraProperties.SensorResolution.THE_800_P,
}
monoResolution = monoResolutionOPT[config['stereodepth']['monoResolution']]

# Default weights to use when blending depth/rgb image (should equal 1.0)
rgbWeight = 0.4
disparityWeight = 0.6

def updateBlendWeights(percent_rgb):
    """
    Update the rgb and disparity weights used to blend disparity/rgb image
    :param percent_rgb : The rgb weight expressed as a percentage (0..100)
    """
    global disparityWeight
    global rgbWeight
    rgbWeight = float(percent_rgb) / 100.0
    disparityWeight = 1.0 - rgbWeight


def updateRange(range):
    """
    Update the maximum range to display in depth window and 3D point cloud visualization
    :param range: Range to apply
    """
    global curRange
    curRange = range

# create the pipeline for stereo depth
pipeline = dai.Pipeline()

monoLeft = pipeline.create(dai.node.MonoCamera)
monoLeft.setResolution(monoResolution)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)

monoRight = pipeline.create(dai.node.MonoCamera)
monoRight.setResolution(monoResolution)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

stereo = pipeline.createStereoDepth()
# stereo = pipeline.create(dai.node.StereoDepth) # alternative to the previous line
stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
stereo.initialConfig.setMedianFilter(median)
stereo.setLeftRightCheck(config['stereodepth']['lrcheck'])
stereo.setExtendedDisparity(config['stereodepth']['extended'])
# https://docs.luxonis.com/projects/api/en/latest/components/nodes/stereo_depth/
stereo.setSubpixel(config['stereodepth']['subpixel'])
print("Default confidence threshold :",stereo.initialConfig.getConfidenceThreshold(),"set to",config['stereodepth']['conf_thr'])
stereo.initialConfig.setConfidenceThreshold(config['stereodepth']['conf_thr'])
monoLeft.out.link(stereo.left)
monoRight.out.link(stereo.right)

config_stereo = stereo.initialConfig.get()
config_stereo.postProcessing.speckleFilter.enable = False
config_stereo.postProcessing.speckleFilter.speckleRange = 50
config_stereo.postProcessing.temporalFilter.enable = config['stereodepth']['temporalFilter']
config_stereo.postProcessing.spatialFilter.enable = config['stereodepth']['spatialFilter']
config_stereo.postProcessing.spatialFilter.holeFillingRadius = 2
config_stereo.postProcessing.spatialFilter.numIterations = 1
config_stereo.postProcessing.thresholdFilter.minRange = config['stereodepth']['minRange']
config_stereo.postProcessing.thresholdFilter.maxRange = config['stereodepth']['maxRange']
config_stereo.postProcessing.decimationFilter.decimationFactor = 1
stereo.initialConfig.set(config_stereo)

xout_depth = pipeline.createXLinkOut()
xout_depth.setStreamName('depth')
stereo.depth.link(xout_depth.input)

xout_disparity = pipeline.createXLinkOut()
xout_disparity.setStreamName('disparity')
stereo.disparity.link(xout_disparity.input)
maxDisparity = stereo.initialConfig.getMaxDisparity()

xout_colorize = pipeline.createXLinkOut()
xout_colorize.setStreamName('colorize')
camRgb = pipeline.create(dai.node.ColorCamera)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setFps(fps)
if config['stereodepth']['downScaleColor']: camRgb.setIspScale(2, 3) #downscale to 720P for RGB camera
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)
camRgb.initialControl.setManualFocus(130)
stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
camRgb.isp.link(xout_colorize.input)

with dai.Device(pipeline) as device:
    print('MxId:', device.getDeviceInfo().getMxId())
    print('USB speed:', device.getUsbSpeed())
    print('Connected cameras:', device.getConnectedCameras())

    qs = []
    qs.append(device.getOutputQueue("depth", 1))
    qs.append(device.getOutputQueue("colorize", 1))
    qs.append(device.getOutputQueue("disparity", 1))

    if show3D:
        try:
            from projector_3d import PointCloudVisualizer
        except ImportError as e:
            show3D = False
            print(
                f"\033[1;5;31mError occured when importing PCL projector: {e}. Point cloud vizualization has been disabled \033[0m ")

    calibData = device.readCalibration()
    w, h = camRgb.getIspSize()
    intrinsics = calibData.getCameraIntrinsics(dai.CameraBoardSocket.RGB, dai.Size2f(w, h))
    if show3D:
        pcl_converter = PointCloudVisualizer(intrinsics, w, h)

    sync = HostSync()
    # create and show windows
    blendedWindowName = "rgb-disparity"
    depthWindowName = "depth"
    if showDisparity and showColor:
        cv2.namedWindow(blendedWindowName)
        cv2.createTrackbar('RGB Weight %', blendedWindowName, int(rgbWeight * 100), 100, updateBlendWeights)
    if showDepth:
        cv2.namedWindow(depthWindowName)
        cv2.createTrackbar('Range (mm)', depthWindowName, curRange, curRange, updateRange)
    # hdf5 file writing
    f = h5py.File(args.file, 'w')
    nb = 0  # frame number
    # group base name in the hdf5 file (group which contains depth, color and disparity)
    grpbase = '%0' + str(config['recording']['nbdigit']) + 'd'
    vts = []        # list of recorded timestamp frames
    duration = 0    # recorded duration
    nbinter = config['display']['ifpsnbframe']  # number of frames needed to compute an instant framerate
    prevtsinter = 0 # previous timestamp for instant framerate computation
    ifps = 0        # instant framerate
    startts = datetime.now().timestamp()    # start recording timestamp
    while True:
        for q in qs:
            new_msg = q.tryGet()    # retrieve frames (i.e msg)
            if new_msg is not None:
                msgs = sync.add_msg(q.getName(), new_msg)
                if msgs:        # new frame
                    ts = datetime.now().timestamp()             # frame timestamp
                    depth = msgs["depth"].getFrame()            # depth
                    color = msgs["colorize"].getCvFrame()       # color image
                    disparity = msgs["disparity"].getFrame()    # disparity
                    vts.append(ts)                              # add timestamp to the timestamps list
                    if showColor:                               # display color image
                        frame = color.copy()
                        if not nb % nbinter:    # compute the instant framerate
                            ifps = nbinter/(ts-prevtsinter)
                            prevtsinter = ts
                        # add some information on the color frame (frame number and instant framerate)
                        frame = cv2.putText(frame, "Recording frame " + str(nb+1) + " @ " + ('%.2f' % ifps) + "fps", (0, 25),
                                            cv2.FONT_HERSHEY_SIMPLEX,
                                            1, (255, 0, 255), 1, cv2.LINE_AA)
                        cv2.imshow("color", frame)
                    if showDisparity:                           # display disparity
                        # Optional, extend range 0..95 -> 0..255, for a better visualisation
                        frameDisp = (disparity * 255. / maxDisparity).astype(np.uint8)
                        # Optional, apply false colorization
                        frameDisp = cv2.applyColorMap(frameDisp, cv2.COLORMAP_HOT)
                        cv2.imshow("disparity", frameDisp)
                        if showColor:                           # display blended disparity with color
                            if len(frameDisp.shape) < 3:
                                frameDisp = cv2.cvtColor(frameDisp, cv2.COLOR_GRAY2BGR)
                            blended = cv2.addWeighted(color, rgbWeight, frameDisp, disparityWeight, 0)
                            cv2.imshow(blendedWindowName, blended)
                    if showDepth:                               # display depth
                        tmpd = np.minimum(curRange, depth)      # remove all depth above curRange
                        # black for far object and white for no value (0) i.e. invalid depths !
                        frameDepth = 255 - ((tmpd * 255. / curRange)).astype(np.uint8)
                        frameDepth = cv2.applyColorMap(frameDepth, cv2.COLORMAP_HOT)
                        cv2.imshow(depthWindowName, frameDepth)
                    nb = nb + 1
                    if len(args.file):      # write frames (datasets) in the hdf5 file
                        dst = f.create_dataset((grpbase + '/depth') % nb, dtype='uint16', data=depth, compression=comp,
                                               compression_opts=comp_opts)
                        if config['recording']['disparity']:
                            dst = f.create_dataset((grpbase + '/disparity') % nb, dtype='uint16', data=disparity,
                                                   compression=comp, compression_opts=comp_opts)
                        if tojpg:   # encode color image in jpg format to take less space
                            colorjpg = cv2.imencode('.jpg', color)[1]
                            dst = f.create_dataset((grpbase + '/color') % nb, data=colorjpg,
                                                   compression=None, compression_opts=None)
                        else:
                            dst = f.create_dataset((grpbase + '/color') % nb, dtype='uint8', data=color,
                                                   compression=comp, compression_opts=comp_opts)
                        f.require_group(grpbase % nb).attrs['ts'] = ts  # timestamp is an attribute of the current frame/dataset
                    duration = ts - startts # compute duration
                    if show3D:  # display a 3D point cloud even if it is not a good idea while recording
                        depth[depth > curRange] = 0 # remove all depth above the currange
                        rgb = cv2.cvtColor(color, cv2.COLOR_BGR2RGB)    # convert BRG to RGB !
                        pcl_converter.rgbd_to_projection(depth, rgb)
                        pcl_converter.visualize_pcd()
        # check if the recording is finished i.e. when key "q" is pressed or number of frames or duration is above a threshold
        if cv2.waitKey(1) == ord('q') or (nb >= nbmax and nbmax) or (duration >= maxduration and maxduration):
            vts = np.array(vts)
            vdts = np.diff(vts)
            duration = vts[-1]-vts[0]   # compute the real recorded duration i.e. from the first recorded frame to the last frame
            meanfps = (nb-1) / duration # compute a mean framerate for the recording
            f.attrs['startts'] = vts[0] # first frame timestamp is considered as recording start
            f.attrs['startdate'] = datetime.fromtimestamp(vts[0]).strftime("%Y-%m-%d %H:%M:%S")
            # copy recording options in the hdf5 file attributes                                                                         )
            for key, value in config['recording'].items():
                if value is None:
                    value = ''
                f.attrs[key] = value
            # copy stereodepth configuration in the hdf5 file attributes
            for key, value in config['stereodepth'].items():
                if value is None:
                    value = ''
                f.attrs[key] = value
            f.attrs['frame_cnt'] = nb       # number of frames in the recording
            f.attrs['duration'] = duration  # duration
            f.attrs['meanfps'] = meanfps    # mean framerate
            f.attrs['maxdts'] = vdts.max()  # maximum time interval in seconds between two frames
            f.attrs['mindts'] = vdts.min()  # minimum time interval in seconds between two frames
            f.attrs['indmaxdts'] = vdts.argmax() + 1    # frame number with maximum time interval between two frames
            f.attrs['indmindts'] = vdts.argmin() + 1    # frame number with minimum time interval between two frames
            f.attrs['intrinsics'] = intrinsics          # camera intrinsics matrix
            f.attrs['camw'] = w                         # camera width (in pixels)
            f.attrs['camh'] = h                         # camera height (in pixels)
            f.attrs['maxDisparity'] = maxDisparity      # maximum disparity value, depends on the configuration
            f.attrs['MxId'] = device.getDeviceInfo().getMxId()  # camera device number
            print("Recording file information", args.file)  # print all hdf5 attributes
            for k, v in f.attrs.items():
                print(k, ":", v)
            f.close()
            print()
            print('Recording to file "'+args.file+'" successfully finished !')
            break
