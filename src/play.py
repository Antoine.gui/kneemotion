#!/usr/bin/env python

"""play.py: play a HDF5 recorded file or a live stream from an OAK-D camera"""

_author__ = "Rudy Ercek"
__copyright__ = "Copyright 2022 - LISA - Université Libre de Bruxelles"
__credits__ = ["Rudy Ercek"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Rudy Ercek"
__email__ = "rudy.ercek@ulb.be"
__status__ = "Production"

import time
import cv2
import numpy as np
import h5py
import json
import argparse
from datetime import datetime
from os.path import exists
from process import DisplayProcess

# Default configuration for the player
config = {
    'display':      # display options while playing
        {
            'disparity': False,     # Hide/show disparity window
            'depth': False,         # Hide/show depth window
            'color': True,          # Hide/show color image
            '3D': False,            # Hide/show 3D point cloud
            'maxRange': 0,          # Maximum depth/range limit to display (0 for full range)
            'ifpsnbframe': 30       # Number of frames to compute an instant framerate shown on the color image
        },
    'playing':      # some playing options (for file only)
        {
            'file': '',             # Filename to play with (default empty --> use camera)
            'speed': 1,             # Play speed to try to reach
            'loop': False           # Play in a loop (repeat from the start)
        },
    'process':      # some process default options
        {
            'colorimgpath': '',     # Path for writing display color images as jpg files
            'colorvideofile': '',   # Video file name for writing color images (e.g. myvideo.avi)
            'colorvideofps': 30,    # Video framerate
            'colorwriteloop': 0,    # Loop number to write video and/or images (0 i.e. first loop !)
        },
    'stereodepth':  # Stereo configuration, see this link (when no file is provided)
                    # https://docs.luxonis.com/projects/api/en/latest/components/nodes/stereo_depth/?highlight=stereodepth#currently-configurable-blocks
        {
            'fps': 30,                  # Color camera framerate (goal to try to reach when recording)
            'conf_thr': 245,            # confidence threshold value 0..255, lower the confidence value means better depth accuracy
                                        # Higher confidence threshold means disparities with less confidence are accepted too.
            'lrcheck': True,            # Better handling for occlusions
            'extended': False,          # Closer-in minimum depth, disparity range is doubled
            'subpixel': True,           # Better accuracy for longer distance, fractional disparity 8-levels
            'monoResolution': '720p',   # The disparity/depth is computed at this resolution, then upscaled to RGB resolution
                                        # (400p, 720p or 800p)
            'downScaleColor': True,     # Optional. If set (True), the ColorCamera is downscaled from 1080p to 720p.
                                        # Otherwise (False), the aligned depth is automatically upscaled to 1080p
            # Filters for depth computation, see the previous link (depth filters) and the one below :
            # https://docs.luxonis.com/projects/api/en/latest/components/messages/stereo_depth_config/#stereodepthconfig
            'minRange': 10,             # Minimum range for depth (mm)
            'maxRange': 15000,          # Maximum range for depth
            'medfilt': '7x7',           # Median filter, 'off','3x3','5x5','7x7'
            'temporalFilter': False,    # Temporal filtering with optional persistence, acquisitions slower in the camera when enabled
            'spatialFilter': False,     # Spatial Filtering using high-order domain transform, acquisitions in the camera slower when enabled
        },
}

# Class to sync all images (depth, color and disparity) from the camera
class HostSync:
    def __init__(self):
        self.arrays = {}

    def add_msg(self, name, msg):
        if not name in self.arrays:
            self.arrays[name] = []
        # Add msg to array
        self.arrays[name].append({'msg': msg, 'seq': msg.getSequenceNum()})
        synced = {}
        for name, arr in self.arrays.items():
            for i, obj in enumerate(arr):
                if msg.getSequenceNum() == obj['seq']:
                    synced[name] = obj['msg']
                    break
        # If there are 3 (all) synced msgs, remove all old msgs
        # and return synced msgs
        if len(synced) == 3:  # color, depth, nn
            # Remove old msgs
            for name, arr in self.arrays.items():
                for i, obj in enumerate(arr):
                    if obj['seq'] < msg.getSequenceNum():
                        arr.remove(obj)
                    else:
                        break
            return synced
        return False


# Arguments parser with some override configuration
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-f', '--file', default='', type=str, help="File to play")
parser.add_argument('-s', '--speed', type=float, help="Speed to play file (e.g. 1, 1.5, ...)")
parser.add_argument('-l', '--loop', type=str, help="Loop file at the end")
parser.add_argument('-fps', '--fps', type=float, help='Color camera framerate (fps) to reach if no file is provided')
parser.add_argument('-c', '--config', type=str, default='config_play.json',
                    help='Configuration json file for playing')
parser.add_argument('-t', '--template', type=str, help='Generate a default json configuration file')

args = parser.parse_args()

# Write the json template for a configuration file
if args.template is not None:
    with open(args.template, 'w') as f:
        json.dump(config, f, indent=4)
        print('A default json configuration file named "'+args.template+'" has been generated ! You can modify it with your settings !')
        exit()  # after creating the template, exit

# Read the json configuration file (if it exists) and merge it with the default configuration
if exists(args.config):
    with open(args.config, 'r') as f:
        cfg = json.load(f)
        for key, value in cfg.items(): # merge json configuration with default configuration (dict)
            if key in config.keys():
                config[key] = {**config[key], **value}
    config['cfgfile'] = args.config
else:
    config['cfgfile'] = ''

# If no file specified by the file argurment, a file to play can be given in the configuration
if args.file == '':
    args.file = config['playing']['file']

# Start playing in live or a file
if args.file == '' or not exists(args.file):    # filename to play not provided or doesn't exist, start realtime playing
    # Stereo Depth configuration
    print('The recorded file has not been provided or does not exist, try to init the camera if it is connected !')
    import depthai as dai
    if config['display']['maxRange'] < 1: # if maxRangeDisplay is not provided, it uses the maxRange filter of stereodepth
        config['display']['maxRange'] = config['stereodepth']['maxRange']
    fps = config['stereodepth']['fps']
    if args.fps is not None: fps = args.fps
    # Median filter Options: MEDIAN_OFF (off), KERNEL_3x3 (3x3), KERNEL_5x5 (5x5), KERNEL_7x7 (7x7)
    median_opt = {
            'off': dai.StereoDepthProperties.MedianFilter.MEDIAN_OFF,
            '3x3': dai.StereoDepthProperties.MedianFilter.KERNEL_3x3,
            '5x5': dai.StereoDepthProperties.MedianFilter.KERNEL_5x5,
            '7x7': dai.StereoDepthProperties.MedianFilter.KERNEL_7x7
    }
    median = median_opt[config['stereodepth']['medfilt']]
    # Resolutions uses for both left and right stereo cameras
    monoResolutionOPT = {
        '400p': dai.MonoCameraProperties.SensorResolution.THE_400_P,
        '480p': dai.MonoCameraProperties.SensorResolution.THE_480_P,
        '720p': dai.MonoCameraProperties.SensorResolution.THE_720_P,
        '800p': dai.MonoCameraProperties.SensorResolution.THE_800_P,
    }
    monoResolution = monoResolutionOPT[config['stereodepth']['monoResolution']]

    # create the pipeline for stereo depth
    pipeline = dai.Pipeline()

    monoLeft = pipeline.create(dai.node.MonoCamera)
    monoLeft.setResolution(monoResolution)
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)

    monoRight = pipeline.create(dai.node.MonoCamera)
    monoRight.setResolution(monoResolution)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

    stereo = pipeline.createStereoDepth()
    stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
    stereo.initialConfig.setMedianFilter(median)
    stereo.setLeftRightCheck(config['stereodepth']['lrcheck'])
    stereo.setExtendedDisparity(config['stereodepth']['extended'])
    # https://docs.luxonis.com/projects/api/en/latest/components/nodes/stereo_depth/
    stereo.setSubpixel(config['stereodepth']['subpixel'])
    print("Default confidence threshold :", stereo.initialConfig.getConfidenceThreshold(), "set to",config['stereodepth']['conf_thr'])
    stereo.initialConfig.setConfidenceThreshold(config['stereodepth']['conf_thr'])
    monoLeft.out.link(stereo.left)
    monoRight.out.link(stereo.right)
    # Post-processing for depth computation
    config_stereo = stereo.initialConfig.get()
    config_stereo.postProcessing.speckleFilter.enable = False
    config_stereo.postProcessing.speckleFilter.speckleRange = 50
    config_stereo.postProcessing.temporalFilter.enable = config['stereodepth']['temporalFilter']
    config_stereo.postProcessing.spatialFilter.enable = config['stereodepth']['spatialFilter']
    config_stereo.postProcessing.spatialFilter.holeFillingRadius = 2
    config_stereo.postProcessing.spatialFilter.numIterations = 1
    config_stereo.postProcessing.thresholdFilter.minRange = config['stereodepth']['minRange']
    config_stereo.postProcessing.thresholdFilter.maxRange = config['stereodepth']['maxRange']
    config_stereo.postProcessing.decimationFilter.decimationFactor = 1
    stereo.initialConfig.set(config_stereo)

    xout_depth = pipeline.createXLinkOut()
    xout_depth.setStreamName('depth')
    stereo.depth.link(xout_depth.input)

    xout_disparity = pipeline.createXLinkOut()
    xout_disparity.setStreamName('disparity')
    stereo.disparity.link(xout_disparity.input)
    maxDisparity = stereo.initialConfig.getMaxDisparity()

    xout_colorize = pipeline.createXLinkOut()
    xout_colorize.setStreamName('colorize')
    camRgb = pipeline.create(dai.node.ColorCamera)
    camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    camRgb.setFps(fps)
    if config['stereodepth']['downScaleColor']: camRgb.setIspScale(2, 3) # downscale to 720P for RGB camera (should be True)
    camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)
    camRgb.initialControl.setManualFocus(130)
    stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
    camRgb.isp.link(xout_colorize.input)

    with dai.Device(pipeline) as device:
        print('MxId:', device.getDeviceInfo().getMxId())
        print('USB speed:', device.getUsbSpeed())
        print('Connected cameras:', device.getConnectedCameras())

        qs = []
        qs.append(device.getOutputQueue("depth", 1))
        qs.append(device.getOutputQueue("colorize", 1))
        qs.append(device.getOutputQueue("disparity", 1))

        calibData = device.readCalibration()
        w, h = camRgb.getIspSize()
        intrinsics = calibData.getCameraIntrinsics(dai.CameraBoardSocket.RGB, dai.Size2f(w, h))
        # Init Display Process
        DProcess = DisplayProcess(config, intrinsics, w, h, maxDisparity)

        sync = HostSync()

        nb = 0
        duration = 0
        startts = datetime.now().timestamp()
        while True:  # infinite loop to retrieve data from the camera
            for q in qs:
                new_msg = q.tryGet()
                if new_msg is not None:
                    msgs = sync.add_msg(q.getName(), new_msg)
                    if msgs:
                        ts = datetime.now().timestamp()
                        depth = msgs["depth"].getFrame()
                        color = msgs["colorize"].getCvFrame()
                        disparity = msgs["disparity"].getFrame()
                        nb = nb + 1
                        DProcess.process(color, depth, disparity, ts, nb)
            if cv2.waitKey(1) == ord('q'):
                break
        duration = ts - startts
        print("Play realtime", nb, "frames during", duration, "seconds at", (nb-1)/duration, "fps")
        DProcess.finish()   # Notification to the display process that the acquisition if finished
else:
    f = h5py.File(args.file, 'r')   # read record file in hdf5 format
    print("Recording file information", args.file)
    for k, v in f.attrs.items():
        print("      ", k, ":", v)
    # Retrieve useful attributes for playing
    nbdigit = f.attrs['nbdigit']
    intrinsics = f.attrs['intrinsics']
    w = f.attrs['camw']
    h = f.attrs['camh']
    maxDisparity = f.attrs['maxDisparity']
    # if the display maxRange is not specified, it uses the range filter value from the file i.e. maxRange
    if config['display']['maxRange'] < 1:
        config['display']['maxRange'] = f.attrs['maxRange']
    tojpg = f.attrs['colortojpg']
    disparity_in_record = f.attrs['disparity']
    if config['display']['disparity'] and not disparity_in_record:
        print("No disparity in the recorded file ! So it cannot be used !")
        config['display']['disparity'] = False
    frame_cnt = f.attrs['frame_cnt']
    # group base name in the hdf5 file (group which contains depth, color and disparity)
    grpbase = '%0' + str(nbdigit) + 'd'
    if args.speed is not None: # override the configuration speed by the argument if specified (not None)
        config['playing']['speed'] = args.speed
    speed = config['playing']['speed']
    if args.loop is not None:
        if args.loop == 'true' or args.loop == 'True' or args.loop == '1':
            config['playing']['loop'] = True
        else:
            config['playing']['loop'] = False
    loop = config['playing']['loop']
    # Init Display Process
    DProcess = DisplayProcess(config, intrinsics, w, h, maxDisparity)
    nbloop = 0  # number of loop
    nb = 1      # current frame number
    rstartts = f.attrs['startts']   # recorded start timestamp
    rfps = f.attrs['meanfps']       # recorded frame rate (mean value)
    dts = 0  # playing duration till the current frame
    drts = 0 # recording duration till the current frame
    pause = False   # Pause the frame
    paused = False  # True if the recording was set in pause during the play
    startts = datetime.now().timestamp()     # play start time
    while nb <= frame_cnt:
        ts = datetime.now().timestamp()
        rts = f.require_group(grpbase % nb).attrs['ts'] #recorded timestamp
        drts = rts - rstartts
        dts = ts-startts
        if drts <= dts*speed: # we want try to play at speed (by default true speed i.e. 1x)
            depth = np.array(f.get((grpbase + '/depth') % nb)[...])
            color = np.array(f.get((grpbase + '/color') % nb)[...])
            if tojpg: # color image in jpg format --> decode it
                color = cv2.imdecode(color, cv2.IMREAD_COLOR)
            if disparity_in_record:
                disparity = np.array(f.get((grpbase + '/disparity') % nb)[...])
            else:
                disparity = np.array([])
            # by default, we still process the frame even in pause, so, some processing parameters could be easily adjusted
            DProcess.process(color, depth, disparity, ts, nb)
            if not pause: # if pause, we do not change to the next frame
                nb = nb + 1
            if loop and nb > frame_cnt: # Reset to the first frame if we repeat the playing (i.e. loop)
                meanfps = (frame_cnt-1) / dts
                nbloop = nbloop+1
                DProcess.finish(nbloop) # notify the display process that a play loop is finished (nbloop <> 0)
                print("Play loop", nbloop, "with", frame_cnt, "frames in", dts, "seconds at", meanfps,
                      "fps that corresponds to",
                      round(100 * meanfps / rfps), "% of the recorded framerate of", rfps, "fps", ('with pause ' if paused else 'without pause'))
                nb = 1
                paused = False
                startts = datetime.now().timestamp()  # play start time

        time.sleep(0.001)   # sleep 1ms to better retrieve key stroke
        k = cv2.waitKey(1)  # get key stroke on an opencv window
        if k == ord('q'):   # key 'q': stop/quit the process
            break
        if k == ord('p'):       # key 'p': play/pause
            pause = not pause   # toggle pause
            paused = True
        if k == ord('n') and pause:     # key 'n': jump to the next frame if pause
            if nb < frame_cnt:
                nb = nb + 1
            else:
                print('Cannot go further because it is last frame of the recording !')
        if k == ord('b') and pause:     # key 'b': jump to the frame before if pause
            if nb == 1:
                print('Cannot go before because it is the first frame of the recording !')
            else:
                nb = nb - 1
    f.close()
    nb = nb - 1
    meanfps = (nb-1)/dts
    print("Finish to play", nb, "frames on a total of", frame_cnt, "in", dts, "seconds at", meanfps, "fps that corresponds to",
          round(100 * meanfps / rfps), "% of the recorded framerate of", rfps, "fps", ('with pause ' if paused else 'without pause'))
    DProcess.finish()   # notify the display process that playing is finished (nbloop not specified, so nbloop = 0)
