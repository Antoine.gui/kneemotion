"""process.py: used for processing and displaying color and depth images from an OAK-D camera"""

__author__ = "Rudy Ercek"
__copyright__ = "Copyright 2022 - LISA - Université Libre de Bruxelles"
__credits__ = ["Rudy Ercek"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Rudy Ercek"
__email__ = "rudy.ercek@ulb.be"
__status__ = "Production"

import cv2
import numpy as np
from datetime import datetime
from scipy.spatial import distance_matrix
import os
import matplotlib.pyplot as plt

class DisplayProcess:
    """
    Class used for processing and displaying color and depth images from an OAK-D camera
    """

    def __init__(self, config, intrinsics, w=1280, h=720, maxDisparity=760):
        """
        Constructor of the class with all settings for the display
        :param config: configuration parameters, mainly for display
        :param intrinsics: intrinsics pinhole camera matrix
        :param w: image width
        :param h: image height
        :param maxDisparity: maximal disparity value for acquired data (usually 760 with 3 bits more precision)
        """
        # main settings
        self.config = config
        self.intrinsics = intrinsics
        self.w = w
        self.h = h
        self.showColor = config['display']['color']
        self.showDepth = config['display']['depth']
        self.showDisparity = config['display']['disparity']
        self.maxRange = config['display']['maxRange']
        self.show3D = config['display']['3D']
        self.maxDisparity = maxDisparity
        self.pcl_converter = None   # Point cloud visualizer
        # Windows name
        self.blendedWindowName = "rgb-disparity"
        self.depthWindowName = "depth"
        self.disparityWindowName = "disparity"
        self.colorWindowName = "color"
        # other internal useful variables
        self.nb = 0     # Current frame number processed
        self.vts = []   # list of time stamp for each processed frame
        self.initts = datetime.now().timestamp()  # a variable to save the timestamp of the initialization
        self.startts = self.initts                # a variable to save the timestamp of the process
        self.nbloop = 0                           # Last loop number finished
        # video/images to write from color data
        self.videoout = None                      # no color video file output !
        self.colorvideofile = self.config['process']['colorvideofile']  # video filename for color image
        self.colorvideofps = self.config['process']['colorvideofps']    # video framerate
        self.colorimgpath = self.config['process']['colorimgpath']      # image path for color images
        # The loop number to write the video and/or images (default 0 i.e. the first one)
        self.colorwriteloop = config['process']['colorwriteloop']
        if len(self.colorimgpath) and not os.path.isdir(self.colorimgpath):
            self.colorimgpath = ''
        # variables in order to compute an instant framerate
        self.nbinter = config['display']['ifpsnbframe']     # Number of frames to compute instant framerate
        self.prevts = 0                                     # previous timestamp (nbinter frames before)
        self.prevnb = 0                                     # previous frame number (=nbinter except first frame !)
        self.ifps = 0                                       # Instant frame rate
        # Default weights to use when blending disparity/rgb image (should equal 1.0)
        self.rgbWeight = 0.4
        self.disparityWeight = 0.6
        self.curRange = self.maxRange   # max depth to display on the depth window (black above)
        if self.showDisparity:
            cv2.namedWindow(self.disparityWindowName)
        if self.showDisparity and self.showColor:
            cv2.namedWindow(self.blendedWindowName)
            cv2.createTrackbar('RGB Weight %', self.blendedWindowName, int(self.rgbWeight * 100), 100, self.updateBlendWeights)
        if self.showDepth:
            cv2.namedWindow(self.depthWindowName)
            cv2.createTrackbar('Range (mm)', self.depthWindowName, self.curRange, self.maxRange, self.updateRange)
        if self.showColor:
            cv2.namedWindow(self.colorWindowName)
        if self.show3D:
            try:
                from projector_3d import PointCloudVisualizer
                self.pcl_converter = PointCloudVisualizer(intrinsics, w, h)
            except ImportError as e:
                print("Error to import the PointCloudVisualizer")
                self.show3D = False
        self.initKneeProcess()

    def initKneeProcess(self):
        """
        This function is called by the constructor, you can initialize your own useful variables and windows
        You should change/adapt the code below and the other two methods: KneepProcess and  finishKneeProcess
        :return:
        """
        # some useful variables are initialized for your process
        self.myvar = 0  # some
        # you can also use the json configuration file to pass some parameters initialization, so you don't have to
        # change them in your code, but in your file (example, configuration specific to a patient and/or exercises)
        # Advise : put your own paramaters in the process section (or create your own)
        if 'myvar' in self.config['process']: # check if 'myvar' key exist in the process configuration (dict)
            self.myvar = self.config['process']['myvar']
            print('"myvar" was specified in the json file with a value of', self.myvar)
        self.mysectionvar = 0   # default value for a specific section variable in the configuration
        if 'mysection' in self.config:  # test if 'mysection' exists in the configuration file
            if 'myvar' in self.config['mysection']: # test if 'myvar' exists in 'mysection'
                self.mysectionvar = self.config['mysection']['myvar']
                print('"myvar" in a new "mysection" was specified in the json file with a value of', self.mysectionvar)
        # you can add your own window and optionally show/hide them using the configuration file
        self.showMyWindow = False       # show/hide MyWindow (default : hide)
        self.MyWindowName = 'MyWindow'  # Name of my window
        if 'mywindow' in self.config['display']:    # check first if 'mywindow' is specified in the display section
            self.showMyWindow = self.config['display']['mywindow']
        self.vout = None    # video out writer for my window
        self.voutfile = ''  # video filename for my window
        self.mywindowimgpath = ''   # writing images path for my window
        self.writemywindow = 0      # loop number to write my video or images
        if self.showMyWindow:
            # create my window
            cv2.namedWindow(self.MyWindowName)
            # init video/images writer
            if 'mywindowvideo' in self.config['process']:  # init video filename if given in config file
                self.voutfile = self.config['process']['mywindowvideo']
            if 'mywindowimgpath' in self.config['process']:# init img file if given in config file
                self.mywindowimgpath = self.config['process']['mywindowimgpath']
                if len(self.mywindowimgpath) and not os.path.isdir(self.mywindowimgpath):  # check if image path exists
                    self.mywindowimgpath = ''
            if 'writemywindow' in self.config['process']:   # check if loop number to write video/image in the config file
                self.writemywindow = self.config['process']['writemywindow']
        
    def updateBlendWeights(self,percent_rgb):
        """
        Update the rgb and disparity weights used to blend disparity/rgb image
        :param percent_rgb: The rgb weight expressed as a percentage (0..100)
        """
        self.rgbWeight = float(percent_rgb) / 100.0
        self.disparityWeight =  1.0 - self.rgbWeight

    def updateRange(self,range):
        """
        Update the maximum visible range
        :param range: the range expressed in mm
        """
        self.curRange = range

    def process(self, color, depth, disparity, ts=0, nb=0):
        """
        Process a frame in order to compute data and/or display images (color/depth, ...)
        :param color: rgb image camera (1280x720x3 uint8)
        :param depth: stereo depth (1280x720x1 uint16)
        :param disparity: stereo disparity (1280x720x1 uint16)
        :param ts: current timestamp of the frame (if 0, automatically generated)
        :param nb: frame number (if 0 automatically computed)
        :return:
        """
        if nb == 0:
            self.nb = self.nb+1
            nb = self.nb
        else:
            self.nb = nb
        if ts == 0:
            ts = datetime.now().timestamp()
        self.vts.append(ts)
        if nb == 1:
            self.startts = ts   # init start of the process
            self.prevts = ts    # init the previous timestamp for the instant fps
            self.prevnb = 1     # frame number for the previous timestamp (prevts)
        if (nb % self.nbinter) == 0:   # compute the instant framerate (self.ifps)
            self.compute_ifps(ts, nb)
        if self.showColor:
            frame = color.copy()
            # add frame number and instant frame rate
            frame = cv2.putText(frame, "Frame " + str(nb) + " @ " + ('%.2f' % self.ifps) + "fps", (0, 25), cv2.FONT_HERSHEY_SIMPLEX,
                                1, (255, 0, 255), 1, cv2.LINE_AA)
            cv2.imshow(self.colorWindowName, frame)
        if self.showDisparity:
            # Optional, extend range 0..95 -> 0..255, for a better visualisation
            frameDisp = (disparity * 255. / self.maxDisparity).astype(np.uint8)
            # Optional, apply false colorization
            frameDisp = cv2.applyColorMap(frameDisp, cv2.COLORMAP_HOT)
            cv2.imshow(self.disparityWindowName, frameDisp)
            if self.showColor:
                if len(frameDisp.shape) < 3:
                    frameDisp = cv2.cvtColor(frameDisp, cv2.COLOR_GRAY2BGR)
                blended = cv2.addWeighted(color, self.rgbWeight, frameDisp, self.disparityWeight, 0)
                cv2.imshow(self.blendedWindowName, blended)
        if self.showDepth:
            tmpd = np.minimum(self.curRange+1, depth)
            # black for far object and white for no volue (0)
            frameDepth = 255 - ((tmpd * 255. / (self.curRange+1))).astype(np.uint8)
            frameDepth = cv2.applyColorMap(frameDepth, cv2.COLORMAP_HOT)
            cv2.imshow(self.depthWindowName, frameDepth)
        if self.pcl_converter is not None:
            tmpd = depth
            tmpd[tmpd > self.curRange] = 0
            rgb = cv2.cvtColor(color, cv2.COLOR_BGR2RGB)
            self.pcl_converter.rgbd_to_projection(tmpd, rgb)
            self.pcl_converter.visualize_pcd()
        # Example where we save a the color frame (with fps)
        if self.nbloop == self.colorwriteloop:
            # create a color video file (if shown) at the first frame
            if nb == 1 and len(self.colorvideofile) and self.showColor:
                sz = frame.shape
                print("Start writing color video file", self.colorvideofile)
                self.videoout = cv2.VideoWriter(self.colorvideofile, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), self.colorvideofps,
                                                (sz[1], sz[0]))
            # write the color image frame if shown and a path/dir is given
            if len(self.colorimgpath) and self.showColor:
                cv2.imwrite((self.colorimgpath + '/%05d.jpg') % self.nb, frame)
        if self.videoout is not None:
            self.videoout.write(frame)
        #self.compute_xyz(depth)
        self.KneeProcess(color, depth, disparity, ts, nb)

    def KneeProcess(self, color, depth, disparity, ts, nb):
        """
        Knee Motion process algorithm with an example code showing the color image with some text on it and optionally
        writing video and/or images
        You should change/adapt the code below and the other two methods: initKneepProcess and  finishKneeProcess
        :param color: color image from the depth camera (BGR uint8)
        :param depth: depth image from the stereo depth camera (uint16)
        :param disparity: disparity image or empty matrix (uint16)
        :param nb: frame number
        :return:
        """
        if self.showMyWindow:       # display mywindow
            frame = color.copy()    # create a copy of the color image to avoid to change the original color image
            # add frame number, instant frame rate and some other information (e.g. filename of the config)
            frame = cv2.putText(frame, "Frame " + str(nb) + " @ " + ('%.2f' % self.ifps) + "fps" + (
                (" - Config File : " + self.config['cfgfile']) if len(
                    self.config['cfgfile']) else "") + " - MyVar:" + str(self.myvar), (0, 25), cv2.FONT_HERSHEY_SIMPLEX,
                                1, (255, 0, 255), 1, cv2.LINE_AA)
            cv2.imshow(self.MyWindowName, frame)    # update the image on my window
            if self.writemywindow == self.nbloop:   # test if it is the loop to write
                if len(self.voutfile) and nb == 1:  # first frame with video file name --> init videowriter
                    sz = frame.shape
                    self.vout = cv2.VideoWriter(self.voutfile, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), self.colorvideofps,
                                                (sz[1], sz[0]))
                if self.vout is not None:
                    self.vout.write(frame)
                if len(self.mywindowimgpath):
                    cv2.imwrite((self.mywindowimgpath + '/%05d.jpg') % nb, frame)
        
    def compute_ifps(self, ts, nb):
        """
        Compute instant framerate normaly every nbinter frames
        :param ts: current timestamp
        :param nb: current framenumber
        :return: instant framerate
        """
        if nb != self.prevnb:
            self.ifps = (nb - self.prevnb) / (ts-self.prevts)
            self.prevts = ts
            self.prevnb = nb
        return self.ifps

    def finish(self, loopnb=0):
        """
        Called after finishing playing or acuiriing
        :param loopnb: loop number (0 if it is not a loop)
        :return:
        """
        self.nbloop = loopnb
        # release/close the video if is was created
        if self.videoout is not None:
            self.videoout.release()
            self.videoout = None
            print("Finish writing color video file", self.colorvideofile)
        self.finishKneeProcess(loopnb)
        # Reset timestamps and frame number
        self.vts = []
        self.nb = 0

    def finishKneeProcess(self,loopnb=0):
        """
        Called by the finish process for the Knee (end of a loop or quit)
        Show results and analysis here during the Knee Motion
        :param loopnb: loop number (0 if it is not a loop and simple the end of the process)
        :return:
        """
        # close video of mywindow
        if self.vout is not None:
            self.vout.release()
            self.vout = None
        # end example
        # Below you can give/display some results to the physiotherapists
