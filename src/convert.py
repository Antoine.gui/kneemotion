#!/usr/bin/env python

"""convert.py: convert a HDF5 recorded file from an OAK-D camera into another HDF5 file or separated files"""

__author__ = "Rudy Ercek"
__copyright__ = "Copyright 2022, LISA - Université Libre de Bruxelles"
__credits__ = ["Rudy Ercek"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Rudy Ercek"
__email__ = "rudy.ercek@ulb.be"
__status__ = "Production"

import json
import cv2
import h5py
import argparse
import os
import numpy as np
from datetime import datetime

# Arguments parser
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
                                 description="Edit, convert recordings to single files or several single files")
parser.add_argument('-f', '--file', default='', type=str, help="Source file to convert with path")
parser.add_argument('-o', '--outfile', default='', type=str, help="Output file with path (hdf5)")
parser.add_argument('-p', '--path', default='', type=str,
                    help="Output directory for color image files (jpg), depth/disparity files (csv) and json "
                         "information file")
parser.add_argument('-c', '--comp', default='none', choices=['none', 'gzip', 'lzf'], help="Compression for file")
parser.add_argument('-l', '--comp_level', default=9, type=int, help="Gzip compression level for hdf5")
parser.add_argument('-s', '--start', type=int, default=1, help='Start frame number for conversion')
parser.add_argument('-e', '--end', type=int, default=0,
                    help='End frame number for conversion (set to 0 for the last frame of the source file)')
parser.add_argument('-d', '--data', default='rgbd', choices=['rgb', 'rgbd', 'all'],
                    help="Data to export, rgb for color image only (not for hdf5), rgbd for depth and image "
                         "and all to add disparity if present")
parser.add_argument('-j', '--jpg', default='idem', choices=['true', 'True', 'false', 'False', 'idem', 'Idem'],
                    help='JPG conversion in hdf5 file, by default idem i.e. same as source file')
args = parser.parse_args()

if len(args.file) == 0 or not os.path.exists(args.file):
    print('The recorded file has not been provided or does not exist !')
    exit()

f = h5py.File(args.file, 'r')  # read source file in hdf5 format
print("Recording file information", args.file)
for k, v in f.attrs.items():
    print("      ", k, ":", v)

if len(args.outfile) == 0 and len(args.path) == 0:
    print("You need to specify an output file or an output path for separated files")
    f.close()
    exit()

if args.end == 0 or args.end > f.attrs['frame_cnt']:
    args.end = f.attrs['frame_cnt'].item()

if args.start > args.end:
    print("The start frame is above the end frame, please correct it !")
    f.close()
    exit()

ish5 = False
if len(args.outfile):
    ish5 = True
    fo = h5py.File(args.outfile, 'w')
    if args.comp == 'none':
        args.comp = None
    comp = args.comp
    comp_opts = None
    if args.comp == 'gzip':
        comp_opts = args.comp_level
else:
    if not os.path.isdir(args.path):
        print("The output path for image and depth map files doesn't exist, please create it !")
        f.close()
        exit()

# retrieve some data for conversion
print()
nbdigit = f.attrs['nbdigit']
tojpg = f.attrs['colortojpg']
toconvert = False  # true if image needs to be converted to jpg or to rgb data in the hdf5 file
if args.jpg != 'idem' and args.jpg != 'Idem' and ish5:
    if args.jpg != 'false' and args.jpg != 'False' and not tojpg:
        toconvert = True
        tojpg = True
    elif tojpg:
        toconvert = True
        tojpg = False

# check which data should be exported
exportdepth = True
exportdisparity = False
if args.data == 'all':
    exportdisparity = True
if args.data == 'rgb':
    exportdepth = False
if exportdisparity:
    exportdisparity = f.attrs['disparity']
elif f.attrs['disparity']:
    print("Disparity exists in the file but it won't be exported !")
if ish5 and not exportdepth:
    print('Depth is always exported in a hdf5 file !')

grpbase = '%0' + str(nbdigit) + 'd'
vts = []    # list which will contain the timestamps of the frames to export
i = 0       # frame number of the exported data
nbtotal = args.end - args.start + 1     # total number of frames to export
nbinter = round(0.05 * nbtotal)         # number of frames to print a progress status (5%)
startcts = datetime.now().timestamp()   # conversion begin timestamp
for nb in range(args.start, args.end + 1):
    rts = f.require_group(grpbase % nb).attrs['ts']  # recorded timestamp (in the file)
    vts.append(rts)
    depth = np.array(f.get((grpbase + '/depth') % nb)[...]) # extract depth data
    color = np.array(f.get((grpbase + '/color') % nb)[...]) # extract image data
    if toconvert:  # check if images should be converted to jpg or raw data
        if tojpg:
            color = cv2.imencode('.jpg', color)[1]
        else:
            color = cv2.imdecode(color, cv2.IMREAD_COLOR)
    if exportdisparity:
        disparity = np.array(f.get((grpbase + '/disparity') % nb)[...])
    i = i + 1
    if (i % nbinter) == 0: # print progress information
        print('Frame', nb, 'converted to frame', i, 'on', nbtotal, 'frames to convert',
              ('(%d%%)' % round(100 * i / nbtotal)), 'after', round(datetime.now().timestamp() - startcts), 'seconds')
    if ish5:
        dst = fo.create_dataset((grpbase + '/depth') % i, dtype='uint16', data=depth, compression=comp,
                                compression_opts=comp_opts)
        if exportdisparity:
            dst = fo.create_dataset((grpbase + '/disparity') % i, dtype='uint16', data=disparity,
                                    compression=comp, compression_opts=comp_opts)
        if tojpg:
            dst = fo.create_dataset((grpbase + '/color') % i, data=color,
                                    compression=None, compression_opts=None)
        else:
            dst = fo.create_dataset((grpbase + '/color') % i, dtype='uint8', data=color,
                                    compression=comp, compression_opts=comp_opts)
        fo.require_group(grpbase % i).attrs['ts'] = rts
    else:
        bfile = args.path + '/' + (grpbase % i)
        jpgfile = bfile + '.jpg'
        if tojpg: # image data are already jpg file, so write raw bytes
            fjpg = open(jpgfile, 'wb')
            fjpg.write(color)
            fjpg.close()
        else: # convert data to an jpg image file
            cv2.imwrite(jpgfile, color)
        if exportdepth:
            np.savetxt(bfile + '.depth.csv', depth, delimiter=',', fmt='%d')
        if exportdisparity:
            np.savetxt(bfile + '.disp.csv', disparity, delimiter=',', fmt='%d')

duration = vts[-1] - vts[0]     # duration of the export
meanfps = (nbtotal-1) / duration    # mean framerate of the export
vdts = np.diff(np.array(vts))
convtime = datetime.now().timestamp() - startcts    # conversion duration
if ish5:
    for k, v in f.attrs.items():
        fo.attrs[k] = v
    # Update some hdf5 file attributes
    fo.attrs['startts'] = vts[0]
    fo.attrs['startdate'] = datetime.fromtimestamp(vts[0]).strftime("%Y-%m-%d %H:%M:%S")
    fo.attrs['frame_cnt'] = nbtotal
    fo.attrs['duration'] = duration
    fo.attrs['meanfps'] = meanfps
    fo.attrs['maxdts'] = vdts.max()
    fo.attrs['mindts'] = vdts.min()
    fo.attrs['indmaxdts'] = vdts.argmax() + 1
    fo.attrs['indmindts'] = vdts.argmin() + 1
    fo.attrs['disparity'] = exportdisparity
    fo.attrs['colortojpg'] = tojpg
    fo.attrs['compression'] = '' if comp is None else comp
    fo.attrs['comp_opts'] = '' if comp_opts is None else comp_opts
    print()
    print("New recording file information", args.outfile)
    for k, v in fo.attrs.items():
        print("      ", k, ":", v)
    print()
    print("Difference between input and output files")
    nbdiff = 0
    convlogdiff = ''
    for k, v in fo.attrs.items():
        if type(v) != np.ndarray and v != f.attrs[k]:
            strdiff = k+": from "+str(f.attrs[k])+" to "+str(v)
            print("      ", strdiff)
            convlogdiff = convlogdiff + strdiff + ';'
            nbdiff = nbdiff + 1
    if nbdiff:
        print("Total:", nbdiff, "differences")
    else:
        print("No difference")
    fo.attrs['src_file'] = args.file
    fo.attrs['convtime'] = convtime
    fo.attrs['convdate'] = startcts
    fo.attrs['convdatestr'] = datetime.fromtimestamp(startcts).strftime("%Y-%m-%d %H:%M:%S")
    fo.attrs['convndbiff'] = nbdiff
    fo.attrs['convlogdiff'] = convlogdiff
    fo.close()
else:
    # create a json information file for the export !
    jsondict = {'file': args.file,
                'export': {
                    'frame_cnt': nbtotal,
                    'frame_start': args.start,
                    'frame_end': args.end,
                    'duration': duration,
                    'disparity': exportdisparity,
                    'depth': exportdepth,
                    'meanfps': meanfps,
                    'convtime': convtime,
                    'convdate': startcts,
                    'convdatestr': datetime.fromtimestamp(startcts).strftime("%Y-%m-%d %H:%M:%S"),
                    'timestamps': vts,
                },
                'info': {},
                }
    for k, v in f.attrs.items():
        t = type(v)
        if t == str:
            jsondict['info'][k] = v
        else: # all attrs type are numpy type --> conversion to json
            if t != np.ndarray:
                jsondict['info'][k] = v.item()
            else:
                jsondict['info'][k] = v.tolist()
    with open(args.path + '/' + 'export.json', 'w') as jsonf:
        json.dump(jsondict, jsonf, indent=4)
print()
print('Finish conversion job after', convtime, 'seconds')
f.close()
